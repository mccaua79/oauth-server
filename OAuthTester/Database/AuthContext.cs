﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace OAuthTester.Database
{
    public class AuthContext : IdentityDbContext<IdentityUser>
    {
        public AuthContext()
            : base("AuthContext")
        {

        }
    }
}